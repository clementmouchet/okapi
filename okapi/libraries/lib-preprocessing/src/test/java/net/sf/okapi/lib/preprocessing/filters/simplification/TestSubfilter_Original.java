package net.sf.okapi.lib.preprocessing.filters.simplification;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterUtil;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.steps.EventLogger;
import net.sf.okapi.lib.extra.steps.TuDpLogger;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestSubfilter_Original {

	private IFilter filter;
	private LocaleId locEN = LocaleId.ENGLISH;
	private FileLocation root;

	@Before
	public void startUp() {
		root = FileLocation.fromClass(getClass());
		filter = FilterUtil.createFilter(
				root.in("/subfilters/okf_xmlstream@microcustom2.fprm").asUrl());
	}
	
	@Test
	public void testEvents() {
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								root.in("/subfilters/import8971089963360986920.xml").asUri(),
								"UTF-8",
								locEN)
						),
						
				new RawDocumentToFilterEventsStep(filter),
				new EventLogger()
		).execute();
	}
	
	@Test
	public void testTuDpEvents() {
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								root.in("/subfilters/import8971089963360986920.xml").asUri(),
								"UTF-8",
								locEN)
						),
						
				new RawDocumentToFilterEventsStep(filter),
				new TuDpLogger()
		).execute();
	}
	
	@Test
	public void testDoubleExtraction() {
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		
		list.add(new InputDocument(root.in("/subfilters/import8971089963360986920.xml").asUri().getPath(), null));
		
		RoundTripComparison rtc = new RoundTripComparison();
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN, "out"));
	}
	
}
