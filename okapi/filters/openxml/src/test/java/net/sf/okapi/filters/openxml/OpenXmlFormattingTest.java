/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.sf.okapi.filters.openxml.OpenXMLTestHelpers.textUnitSourceExtractor;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class OpenXmlFormattingTest {

    private final LocaleId locENUS = LocaleId.fromString("en-us");
    private final FileLocation root = FileLocation.fromClass(getClass());

    @Test
    public void extractsItalics() {
        ConditionalParameters params = new ConditionalParameters();
        params.setTranslateDocProperties(false);
        OpenXMLFilter filter = new OpenXMLFilter();

        RawDocument doc = new RawDocument(root.in("/formatting/italics2.docx").asUri(), StandardCharsets.UTF_8.name(), locENUS);
        ArrayList<Event> events = FilterTestDriver.getEvents(filter, doc, params);
        List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(events);
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
                "<run1>This text has italics.</run1><run2> This text is not in italics.</run2>"
        );

        // The first sentence should have italics; the second should not
        assertThat(
                textUnits.get(0).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(4).extracting("type").containsExactly(
                "x-italic;color:000000;highlight:auto;",
                "x-italic;color:000000;highlight:auto;",
                "x-color:000000;highlight:auto;",
                "x-color:000000;highlight:auto;"
        );
    }

    @Test
    public void extractsCaps() {
        final RawDocument doc = new RawDocument(root.in("/formatting/784.docx").asUri(), StandardCharsets.UTF_8.name(), locENUS);
        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(
            FilterTestDriver.getEvents(new OpenXMLFilter(), doc, new ConditionalParameters())
        );
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
            "<run1>CAPS property</run1> and UPPERCASE CHARACTERS.",
            "User"
        );
        assertThat(
                textUnits.get(0).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-caps;",
            "x-caps;"
        );
    }

    @Test
    public void extractsHighlightAndShade() {
        final RawDocument doc = new RawDocument(root.in("/formatting/790.docx").asUri(), StandardCharsets.UTF_8.name(), locENUS);
        final List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(
                FilterTestDriver.getEvents(new OpenXMLFilter(), doc, new ConditionalParameters())
        );
        assertThat(textUnits).extracting(textUnitSourceExtractor()).containsExactly(
            "Highlight variations: <run1>one</run1>, <run2>two</run2> and three.",
            "Shade variations: <run1>one</run1>, <run2>two</run2> and <run3>three</run3>.",
            "A shaded paragraph with <run1>highlighted characters</run1>.",
            "<run1>Shaded and highlighted.</run1>",
            "User"
        );
        assertThat(
            textUnits.get(0).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(4).extracting("type").containsExactly(
            "x-highlight:darkGray;",
            "x-highlight:darkGray;",
            "x-highlight:lightGray;",
            "x-highlight:lightGray;"
        );
        assertThat(
            textUnits.get(1).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(6).extracting("type").containsExactly(
            "x-highlight:7F7F7F;",
            "x-highlight:7F7F7F;",
            "x-highlight:BFBFBF;",
            "x-highlight:BFBFBF;",
            "x-highlight:FFFFFF;",
            "x-highlight:FFFFFF;"
        );
        assertThat(
            textUnits.get(2).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-highlight:darkGray;",
            "x-highlight:darkGray;"
        );
        assertThat(
            textUnits.get(3).getSource().getParts().get(0).getContent().getCodes()
        ).hasSize(2).extracting("type").containsExactly(
            "x-highlight:darkGray;",
            "x-highlight:darkGray;"
        );
    }
}
