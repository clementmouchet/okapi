/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.SubFilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createGeneralMarkupComponent;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isStringItemStartEvent;

class SharedStringsPartHandler extends GenericPartHandler {
    private final StyleDefinitions styleDefinitions;
    private final EncoderManager encoderManager;
    private final IFilter subfilter;
    private final SharedStringMap sharedStringMap;

    private IdGenerator textUnitId;
    private IdGenerator nestedBlockId;

    private XMLEventReader xmlReader;
    private Iterator<Event> filterEventIterator;
    private String docId;
    private String subDocId;
    private LocaleId sourceLocale;
    private File rewrittenStringsTable;
    private int sharedStringIndex = 0;

    SharedStringsPartHandler(ConditionalParameters cparams, OpenXMLZipFile zipFile, ZipEntry entry, StyleDefinitions styleDefinitions,
                             EncoderManager encoderManager, IFilter subfilter, SharedStringMap sharedStringMap) {
        super(cparams, entry.getName());
        this.zipFile = zipFile;
        this.entry = entry;
        this.styleDefinitions = styleDefinitions;
        this.encoderManager = encoderManager;
        this.subfilter = subfilter;
        this.sharedStringMap = sharedStringMap;

        textUnitId = new IdGenerator(partName, IdGenerator.TEXT_UNIT);
        nestedBlockId = new IdGenerator(null);
        markup = new Block.BlockMarkup();
    }

    /**
     * Open this part and perform any initial processing.  Return the
     * first event for this part.  In this case, it's a START_SUBDOCUMENT
     * event.
     *
     * @param docId    document identifier
     * @param subDocId sub-document identifier
     * @param sourceLocale the locale of the source
     * @return Event
     * @throws IOException
     * @throws XMLStreamException
     */
    @Override
    public Event open(String docId, String subDocId, LocaleId sourceLocale) throws IOException, XMLStreamException {
        this.docId = docId;
        this.subDocId = subDocId;
        this.sourceLocale = sourceLocale;

        SharedStringsDenormalizer deno = new SharedStringsDenormalizer(zipFile.getEventFactory(), sharedStringMap);
        XMLEventReader reader = zipFile.getInputFactory().createXMLEventReader(
                new InputStreamReader(zipFile.getInputStream(entry), StandardCharsets.UTF_8));
        rewrittenStringsTable = File.createTempFile("sharedStrings", ".xml");
        XMLEventWriter writer = zipFile.getOutputFactory().createXMLEventWriter(
                new OutputStreamWriter(new FileOutputStream(rewrittenStringsTable), StandardCharsets.UTF_8));
        deno.process(reader, writer);

        InputStream is = new BufferedInputStream(new FileInputStream(rewrittenStringsTable));

        this.xmlReader = XMLInputFactory.newInstance().createXMLEventReader(is);
        try {
            process();
        } finally {
            if (xmlReader != null) {
                xmlReader.close();
            }
            rewrittenStringsTable.delete();
        }
        return createStartSubDocumentEvent(docId, subDocId);
    }

    private void process() throws XMLStreamException {
        XMLEvent e = null;
        while (xmlReader.hasNext()) {
            e = xmlReader.nextEvent();
            if (isStringItemStartEvent(e) && sharedStringMap.isStringVisible(sharedStringIndex++)) {
                flushDocumentPart();
                StartElementContext startElementContext = createStartElementContext(e.asStartElement(), xmlReader, zipFile.getEventFactory(), params);
                StringItem stringItem = new StringItemParser(startElementContext, nestedBlockId, styleDefinitions).parse();

                final List<ITextUnit> textUnits = new StringItemTextUnitMapper(stringItem, textUnitId).map();
                if (textUnits.isEmpty()) {
                    addBlockChunksToDocumentPart(stringItem.getChunks());
                } else {
                    if (subfilter != null && !stringItem.isStyled()) {
                        addSubfilteredEvents(textUnits);
                    } else {
                        addTextUnitEvents(textUnits);
                    }
                }
            } else {
                addEventToDocumentPart(e);
            }
        }
        flushDocumentPart();
        filterEvents.add(new Event(EventType.END_DOCUMENT, new Ending(subDocId)));
        filterEventIterator = filterEvents.iterator();
    }

    private void addTextUnitEvents(final List<ITextUnit> textUnits) {
        for (final ITextUnit tu : textUnits) {
            filterEvents.add(new Event(EventType.TEXT_UNIT, tu));
        }
    }

    private void addSubfilteredEvents(final List<ITextUnit> textUnits) {
        int subfilterIndex = 0;
        for (final ITextUnit tu : textUnits) {
            try (final SubFilter sf = new SubFilter(subfilter, encoderManager.getEncoder(), ++subfilterIndex, tu.getId(), tu.getName())) {
                filterEvents.addAll(sf.getEvents(new RawDocument(tu.getSource().getFirstContent().getText(), sourceLocale)));
                filterEvents.add(sf.createRefEvent(tu));
            }
        }
    }

    private void addMarkupComponentToDocumentPart(MarkupComponent markupComponent) {
        if (!documentPartEvents.isEmpty()) {
            markup.addComponent(createGeneralMarkupComponent(documentPartEvents));
            documentPartEvents = new ArrayList<>();
        }
        markup.addComponent(markupComponent);
    }

    private void addBlockChunksToDocumentPart(List<Chunk> chunks) {
        for (Chunk chunk : chunks) {
            if (chunk instanceof Markup) {
                for (MarkupComponent markupComponent : ((Markup) chunk).getComponents()) {
                    addMarkupComponentToDocumentPart(markupComponent);
                }
                continue;
            }

            documentPartEvents.addAll(chunk.getEvents());
        }
    }

    @Override
    public boolean hasNext() {
        return filterEventIterator.hasNext();
    }

    @Override
    public Event next() {
        return filterEventIterator.next();
    }

    @Override
    public void close() {
    }

    @Override
    public void logEvent(Event e) {
    }
}
