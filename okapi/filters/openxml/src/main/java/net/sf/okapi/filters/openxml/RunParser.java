/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.TextFragment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripParagraphRevisionAttributes;
import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripRunRevisionAttributes;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;
import static net.sf.okapi.filters.openxml.StyleExclusionChecker.isStyleExcluded;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_REGULAR_HYPHEN_VALUE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.hasPreserveWhitespace;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isComplexCodeEnd;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isComplexCodeSeparate;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isComplexCodeStart;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isFieldCodeEndEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isFieldCodeStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isGraphicsProperty;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isLineBreakStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isNoBreakHyphenStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isPageBreak;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isParagraphStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isRunPropsStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isRunStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTabStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextPath;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;

class RunParser implements Parser<RunBuilder> {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final StartElementContext startElementContext;
    private final IdGenerator nestedTextualIds;

    private final Deque<ComplexCodeProcessingState> nestedComplexCodeProcessingStates;

    private final RunBuilder runBuilder;

    private final ElementSkipper elementSkipper;

    private final RunSkippableElements runSkippableElements;
    private final BlockSkippableElements blockSkippableElements;

    RunParser(StartElementContext startElementContext, IdGenerator nestedTextualIds, StyleDefinitions styleDefinitions, boolean hidden) {
        this.startElementContext = createStartElementContext(stripRunRevisionAttributes(startElementContext), startElementContext);
        this.nestedTextualIds = nestedTextualIds;

        nestedComplexCodeProcessingStates = new LinkedList<>();

        runBuilder = new RunBuilder(this.startElementContext, styleDefinitions);
        runBuilder.setHidden(hidden);

        elementSkipper = ElementSkipperFactory.createGeneralElementSkipper(startElementContext.getConditionalParameters());

        runSkippableElements = new RunSkippableElements(startElementContext);
        blockSkippableElements = new BlockSkippableElements(startElementContext);
    }

    public RunBuilder parse() throws XMLStreamException {
        log("startRun: " + startElementContext.getStartElement());

        // rPr is either the first child, or not present (section 17.3.2)
        XMLEvent firstChild = startElementContext.getEventReader().nextTag();
        if (isRunPropsStartEvent(firstChild)) {
            parseRunProperties(firstChild.asStartElement());
        } else if (isEndElement(firstChild, startElementContext.getStartElement())) {
            // Empty run!
            return endRunParsing(firstChild.asEndElement());
        } else {
            // No properties section
            startRunParsing(firstChild);
        }
        while (startElementContext.getEventReader().hasNext()) {
            XMLEvent e = startElementContext.getEventReader().nextEvent();
            log("processRun: " + e);
            if (isEndElement(e, startElementContext.getStartElement())) {
                return endRunParsing(e.asEndElement());
            } else {
                startRunParsing(e);
            }
        }
        throw new IllegalStateException("Invalid content? Unterminated run");
    }

    private RunBuilder endRunParsing(EndElement e) {
        runBuilder.flushText();
        runBuilder.flushMarkupChunk();
        this.runBuilder.setEndEvent(e);
        // XXX This is pretty hacky.
        // Recalculate the properties now that consolidation has already happened.
        // This is required in order to properly handle the aggressive-mode trimming
        // of the vertAlign property, which is only done if there's no text in the
        // run.  Whether or not text is present can only be correctly calculated
        // -after- other run merging has already taken place.
        if (!runBuilder.hasNonWhitespaceText() && startElementContext.getConditionalParameters().getCleanupAggressively()) {
            runBuilder.setRunProperties(RunProperties.copiedRunProperties(runBuilder.getRunProperties(), true, false, false));
        }
        return this.runBuilder;
    }

    private void startRunParsing(final XMLEvent e) throws XMLStreamException {
        if (isParagraphStartEvent(e)) {
            parseNestedBlock(e);
        }
        // XXX I need to make sure I don't try to merge this thing
        else if (isComplexCodeStart(e)) {
            parseComplexCodes(e.asStartElement());
        } else {
            if (!parseSkippableElements(e)) {
                parseContent(processTranslatableAttributes(e));
            }
        }
    }

    private void parseRunProperties(StartElement startElement) throws XMLStreamException {
        StartElementContext runPropertiesElementContext = createStartElementContext(startElement, runBuilder.getStartElementContext());
        runBuilder.setRunProperties(new RunPropertiesParser(runPropertiesElementContext, runSkippableElements).parse());

        RunProperty.RunStyleProperty runStyleProperty = runBuilder.getRunProperties().getRunStyleProperty();

        if (null == runStyleProperty) {
            return;
        }

        runBuilder.setRunStyle(runStyleProperty.getValue());
        runBuilder.setHidden(isStyleExcluded(runStyleProperty.getValue(), startElementContext.getConditionalParameters()));
    }

    private void parseNestedBlock(XMLEvent e) throws XMLStreamException {
        log("Nested block start event: " + e);
        runBuilder.flushText();
        StartElementContext blockElementContext = createStartElementContext(e.asStartElement(), startElementContext);

        BlockParser nestedBlockParser = new BlockParser(blockElementContext, nestedTextualIds, runBuilder.getStyleDefinitions());
        Block nested = nestedBlockParser.parse();
        runBuilder.setContainsNestedItems(true);
        if (nested.hasVisibleRunContent()) {
            // Create a reference to mark the location of the nested block
            runBuilder.addToMarkupChunk(startElementContext.getEventFactory().createCharacters(
                    TextFragment.makeRefMarker(nestedTextualIds.createId())));
            runBuilder.getNestedTextualItems().add(nested);
        } else {
            // Empty block, we don't need to expose it after all
            for (XMLEvent nestedEvent : nested.getEvents()) {
                runBuilder.addToMarkupChunk(nestedEvent);
            }
            // However, we do need to preserve anything it references that's translatable
            for (Chunk chunk : nested.getChunks()) {
                if (chunk instanceof Run) {
                    runBuilder.getNestedTextualItems().addAll(((Run) chunk).getNestedTextualItems());
                } else if (chunk instanceof RunContainer) {
                    for (Block.BlockChunk nestedChunk : ((RunContainer) chunk).getChunks()) {
                        if (nestedChunk instanceof Run) {
                            runBuilder.getNestedTextualItems().addAll(((Run) nestedChunk).getNestedTextualItems());
                        }
                    }
                }
            }
        }
    }

    private void parseComplexCodes(final StartElement startElement) throws XMLStreamException {
        boolean isFieldCodeValue = false;
        nestedComplexCodeProcessingStates.add(new ComplexCodeProcessingState());
        runBuilder.setHasComplexCodes(true);
        runBuilder.addToMarkupChunk(startElement);

        while (startElementContext.getEventReader().hasNext()) {
            final XMLEvent e = (XMLEvent) startElementContext.getEventReader().next();
            if (nestedComplexCodeProcessingStates.peek().isAfterSeparate()
                    && nestedComplexCodeProcessingStates.peek().containsPersistentContent()
                    && !isComplexCodeEnd(e)
                    && !isComplexCodeStart(e)) {
                if (parseSkippableElements(e)) {
                    continue;
                }
                parseContent(processTranslatableAttributes(stripRevisionAttributes(e)));
            } else {
                if (parseSkippableElements(e)) {
                    continue;
                }
                runBuilder.addToMarkupChunk(stripRevisionAttributes(e));

                if (isComplexCodeStart(e)) {
                    nestedComplexCodeProcessingStates.add(new ComplexCodeProcessingState());
                } else if (isComplexCodeSeparate(e)) {
                    nestedComplexCodeProcessingStates.peekLast().setAfterSeparate(true);
                } else if (isComplexCodeEnd(e)) {
                    nestedComplexCodeProcessingStates.pollLast();
                    if (nestedComplexCodeProcessingStates.peekLast() == null) {
                        break;
                    }
                } else if (!nestedComplexCodeProcessingStates.peek().isAfterSeparate()) {
                    if (isFieldCodeStartEvent(e)) {
                        isFieldCodeValue = true;
                    } else if (isFieldCodeValue && isFieldCodeWithPersistentContent(e)) {
                        nestedComplexCodeProcessingStates.peek().setContainsPersistentContent(true);
                    } else if (isFieldCodeEndEvent(e)) {
                        isFieldCodeValue = false;
                    }
                }
            }
        }
    }

    private XMLEvent stripRevisionAttributes(XMLEvent e) {
        if (isParagraphStartEvent(e)) {
            return stripParagraphRevisionAttributes(createStartElementContext(e.asStartElement(), startElementContext));
        } else if (isRunStartEvent(e)) {
            return stripRunRevisionAttributes(createStartElementContext(e.asStartElement(), startElementContext));
        } else {
            return e;
        }
    }

    private boolean parseSkippableElements(final XMLEvent e) throws XMLStreamException {
        if (runSkippableElements.skip(e)) {
            return true;
        } else if (blockSkippableElements.skip(e)) {
            return true;
        }
        return false;
    }

    private void parseContent(final XMLEvent e) throws XMLStreamException {

        if (isTextStartEvent(e)) {

            runBuilder.flushMarkupChunk();
            parseText(e.asStartElement(), startElementContext.getEventReader());

        } else if (startElementContext.getConditionalParameters().getAddTabAsCharacter() && isTabStartEvent(e) && !runBuilder.isHidden()) {

            runBuilder.flushMarkupChunk();
            runBuilder.addText("\t", e.asStartElement());
            elementSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext.getEventReader(), null, startElementContext.getConditionalParameters()));

        } else if (startElementContext.getConditionalParameters().getAddLineSeparatorCharacter() && isLineBreakStartEvent(e) && !isPageBreak(e.asStartElement()) && !runBuilder.isHidden()) {

            runBuilder.flushMarkupChunk();
            char replacement = startElementContext.getConditionalParameters().getLineSeparatorReplacement();
            runBuilder.addText(String.valueOf(replacement), e.asStartElement());
            elementSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext.getEventReader(), null, startElementContext.getConditionalParameters()));

        } else if (startElementContext.getConditionalParameters().getReplaceNoBreakHyphenTag() && isNoBreakHyphenStartEvent(e)) {

            runBuilder.flushMarkupChunk();
            runBuilder.addText(LOCAL_REGULAR_HYPHEN_VALUE, e.asStartElement());
            elementSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext.getEventReader(), null, startElementContext.getConditionalParameters()));

        } else if (!isWhitespace(e) || runBuilder.preservingWhitespace()) {

            // Real text should have been handled above.  Most whitespace is ignorable,
            // but if we're in a preserve-whitespace section, we need to save it (eg
            // for w:instrText, which isn't translatable but needs to be preserved).
            runBuilder.flushText();
            runBuilder.setTextPreservingWhitespace(false);
            runBuilder.addToMarkupChunk(e);
        }
    }

    private void parseText(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
        // Merge the preserve whitespace flag
        runBuilder.setTextPreservingWhitespace(runBuilder.isTextPreservingWhitespace() || hasPreserveWhitespace(startEvent));

        while (events.hasNext()) {
            XMLEvent e = events.nextEvent();
            if (isEndElement(e, startEvent)) {
                return;
            } else if (e.isCharacters()) {
                String text = e.asCharacters().getData();
                if (text.trim().length() > 0) {
                    runBuilder.setNonWhitespaceText(true);
                }
                runBuilder.addText(text, startEvent);
            }
        }
    }

    private boolean isFieldCodeWithPersistentContent(XMLEvent e) {
        if (e.isCharacters()) {
            //get the field definition out of the field code string
            String data = e.asCharacters().getData().trim();
            int fieldCodeNameLength = data.indexOf(" ");

            String fieldCodeName;
            if (fieldCodeNameLength > 0) {
                fieldCodeName = data.substring(0, fieldCodeNameLength);
            } else {
                fieldCodeName = data;
            }

            return startElementContext.getConditionalParameters().tsComplexFieldDefinitionsToExtract.contains(fieldCodeName);
        } else {
            return false;
        }
    }

    // translatable attributes:
    // wp:docPr/@name  if that option isn't set
    // v:textpath/@string
    private XMLEvent processTranslatableAttributes(XMLEvent e) {
        if (!e.isStartElement()) return e;
        StartElement startEl = e.asStartElement();
        // I will need to
        // - extract translatable attribute
        // - create a new start event with all the attributes except for that one, which is replaced
        if (isGraphicsProperty(startEl) && !startElementContext.getConditionalParameters().getTranslateWordExcludeGraphicMetaData()) {
            startEl = processTranslatableAttribute(startEl, "name");
        } else if (isTextPath(startEl)) {
            startEl = processTranslatableAttribute(startEl, "string");
        }
        return startEl;
    }

    private StartElement processTranslatableAttribute(StartElement startEl, String attrName) {
        List<Attribute> newAttrs = new ArrayList<>();
        Iterator<?> it = startEl.getAttributes();
        boolean dirty = false;
        while (it.hasNext()) {
            Attribute a = (Attribute) it.next();
            if (a.getName().getLocalPart().equals(attrName)) {
                runBuilder.setContainsNestedItems(true);
                runBuilder.getNestedTextualItems().add(new TranslatableAttributeText(a.getValue()));
                newAttrs.add(startElementContext.getEventFactory().createAttribute(a.getName(),
                        TextFragment.makeRefMarker(nestedTextualIds.createId())));
                dirty = true;
            } else {
                newAttrs.add(a);
            }
        }
        return dirty ?
                startElementContext.getEventFactory().createStartElement(startEl.getName(), newAttrs.iterator(), startEl.getNamespaces()) :
                startEl;
    }

    private void log(String s) {
        LOGGER.debug(s);
    }

    private static class ComplexCodeProcessingState {
        private Boolean containsPersistentContent = false;
        private Boolean afterSeparate = false;

        Boolean containsPersistentContent() {
            return containsPersistentContent;
        }

        void setContainsPersistentContent(Boolean containsPersistentContent) {
            this.containsPersistentContent = containsPersistentContent;
        }

        Boolean isAfterSeparate() {
            return afterSeparate;
        }

        void setAfterSeparate(Boolean afterSeparate) {
            this.afterSeparate = afterSeparate;
        }
    }
}
